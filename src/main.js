import Vue from "vue";
import Vuelidate from "vuelidate";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "materialize-css/dist/js/materialize.min";

import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

Vue.config.productionTip = false;
Vue.use(Vuelidate);

firebase.initializeApp({
  apiKey: "AIzaSyB0O093mdkFmsmiyNv0TVvO6y0FdnTzfTY",
  authDomain: "apiko-97e13.firebaseapp.com",
  databaseURL:
    "https://apiko-97e13-default-rtdb.europe-west1.firebasedatabase.app/",
  projectId: "apiko-97e13",
  storageBucket: "apiko-97e13.appspot.com",
  messagingSenderId: "736217311295",
  appId: "1:736217311295:web:9b85b724dcf92943c2308a"
});

let app;
firebase.auth().onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App),
      created() {
        if (user) {
          this.$store.dispatch("autoSignIn", user);
        }
      }
    }).$mount("#app");
  }
});
