import Vue from "vue";
import VueRouter from "vue-router";
import firebase from "firebase/app";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/Register.vue")
  },
  {
    path: "/inbox",
    name: "Inbox",
    meta: { secure: true, navBarTheme: "inbox" },
    component: () => import("../views/Inbox.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

const authPages = ["Login", "Register"];

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser;
  const requireAuth = to.matched.some(record => record.meta.secure);
  if (requireAuth && !currentUser) {
    next("/login");
  } else {
    if (currentUser && authPages.indexOf(to.name) > -1) next("/");
    else next();
  }
});

export default router;
