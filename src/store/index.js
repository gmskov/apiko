import Vue from "vue";
import Vuex from "vuex";
import firebase from "firebase/app";
import M from "materialize-css";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    loading: false,
    items: [],
    favoriteItems: [],
    showFavorites: false
  },
  mutations: {
    createItem(state, payload) {
      state.items.push(payload);
    },
    setShowFavorites(state, payload) {
      state.showFavorites = payload;
    },
    setItems(state, payload) {
      state.items = payload;
    },
    setFavorites(state, payload) {
      state.favoriteItems = payload;
    },
    updateFavorite(state, payload) {
      if (payload.favorite) {
        state.favoriteItems.push(payload);
      } else {
        state.favoriteItems = state.favoriteItems.filter(
          item => item.id !== payload.id
        );
      }
    },
    setUser(state, payload) {
      state.user = payload;
    },
    unsetUser(state) {
      state.user = null;
    },
    setLoading(state, payload) {
      state.loading = payload;
    }
  },
  actions: {
    getItems: function({ commit }) {
      let items = [];
      let favorites = [];
      return firebase
        .database()
        .ref("items/")
        .once("value")
        .then(data => {
          const value = data.val();
          for (const key in value) {
            value[key].id = key;
            items.push(value[key]);
            if (value[key].favorite) favorites.push(value[key]);
          }
          commit("setItems", items);
          commit("setFavorites", favorites);
        });
    },
    getFavorites({ commit }) {
      let items = [];
      return firebase
        .database()
        .ref("favorites/")
        .once("value")
        .then(data => {
          const value = data.val();
          for (const key in value) {
            value[key].id = key;
            items.push(value[key]);
          }
          commit("setItems", items);
        });
    },
    createItem({ commit }, payload) {
      const item = {
        title: payload.title,
        location: payload.location,
        description: payload.description,
        imageUrl: payload.imageUrl,
        price: payload.price,
        userId: payload.userId
      };
      let imageUrl;
      let key;
      return firebase
        .database()
        .ref("items")
        .push(item)
        .then(data => {
          key = data.key;
          return key;
        })
        .then(key => {
          if (!payload.image) return;
          let filename = payload.image.name;
          let ext = filename.substr(filename.lastIndexOf("."));
          return firebase
            .storage()
            .ref("items/" + key + ext)
            .put(payload.image);
        })
        .then(async fileData => {
          //imageUrl = fileData.metadata.downloadURLs[0];
          imageUrl = await firebase
            .storage()
            .ref()
            .child(fileData.ref.fullPath)
            .getDownloadURL();
          return firebase
            .database()
            .ref("items")
            .child(key)
            .update({ imageUrl: imageUrl });
        })
        .then(() => {
          commit("createItem", {
            ...item,
            imageUrl: imageUrl,
            id: key
          });
        });
    },
    registerUser({ commit }, payload) {
      firebase
        .auth()
        .createUserWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          const newUser = {
            id: response.user.uid
          };
          commit("setUser", newUser);
        })
        .catch(error => {
          M.toast({ html: error.message });
        });
    },
    signUserIn({ commit }, payload) {
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password)
        .then(response => {
          const newUser = {
            id: response.user.uid
          };
          commit("setUser", newUser);
        })
        .catch(error => {
          M.toast({ html: error.message });
        });
    },
    autoSignIn({ commit }, payload) {
      commit("setUser", { id: payload.uid });
    },
    unsetUser({ commit }) {
      firebase
        .auth()
        .signOut()
        .then(() => {
          commit("unsetUser");
        });
    },
    setLoading({ commit }, payload) {
      commit("setLoading", payload);
    },
    updateFavorite({ commit }, payload) {
      firebase
        .database()
        .ref("items/" + payload.id)
        .update({
          favorite: payload.favorite
        })
        .then(() => {
          commit("updateFavorite", payload);
        });
    }
  },
  getters: {
    user(state) {
      return state.user;
    },
    loading(state) {
      return state.loading;
    },
    getItems(state) {
      return state.items;
    },
    getShowFavorites(state) {
      return state.showFavorites;
    },
    getFavoriteItems(state) {
      return state.favoriteItems;
    }
  },
  modules: {}
});
